import React, {Component} from 'react'
import './Content.css'
import axios from '../../axios-layout'
import Spinner from "../../components/UI/Spinner/Spinner";

class Content extends Component {
	state = {
		title: '',
		content: '',
		loading: true,
		page: null
	};
	
	componentDidMount() {
		this.requestPageInfo();
	}
	
	componentDidUpdate() {
		let page = this.props.match.params.id;
		if (page !== this.state.page) {
			this.setState({page});
			this.requestPageInfo();
		}
	}
	
	requestPageInfo = async() => {
		let page = null;
		this.props.match.params.id ? page = this.props.match.params.id : page = 'about';
		let i = await axios.get(`/${page}.json`).finally(() => {
			this.setState({loading: false});
		});
		this.setState({title: i.data.title, content: i.data.content});
	};
	
	render() {
		let content = (
			<div className="ContentPage">
				<h1>{this.state.title}</h1>
				<div className="text">
					{this.state.content}
				</div>
			</div>
		);
		return this.state.loading ? <Spinner/> : content;
	}
}

export default Content;