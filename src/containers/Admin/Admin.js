import React, {Component, Fragment} from 'react'
import axios from '../../axios-layout'
import Spinner from "../../components/UI/Spinner/Spinner";
import './Admin.css'

class Admin extends Component {
	state = {
		selectValue: 'about',
		page: {
			title: '',
			content: '',
		},
		loading: true,
		name: null
	};
	
	componentDidMount() {
		this.requestPageInfo();
	}
	
	componentDidUpdate() {
		if (this.state.name !== this.state.selectValue) {
			this.requestPageInfo();
		}
	}
	
	requestPageInfo = async () => {
		let name = this.state.selectValue;
		let page = {...this.state.page};
		let i = await axios.get(`/${name}.json`).finally(() => {
			this.setState({loading: false});
		});
		page = {title: i.data.title, content: i.data.content};
		this.setState({page, name});
	};
	
	selectPage = (e) => {
		let selectValue = this.state.selectValue;
		selectValue = e.target.value;
		this.setState({selectValue});
	};
	
	saveHandler = (e) => {
		e.preventDefault();
		axios.put(`/${this.state.name}.json`, this.state.page).then(() => {
			this.props.history.replace(`/${this.state.name}`);
		});
	};
	
	changeTitle = (e) => {
		let page = {...this.state.page};
		page.title = e.target.value;
		this.setState({page});
	};
	
	changeContent = (e) => {
		let page = {...this.state.page};
		page.content = e.target.value;
		this.setState({page});
	};
	
	render() {
		let info = (
			<Fragment>
				<label htmlFor="select-page">Choose page</label>
				<select id="select-page" value={this.state.selectValue} onChange={(e) => this.selectPage(e)}>
					<option value="about">about</option>
					<option value="contacts">contacts</option>
					<option value="ipad">ipad</option>
					<option value="iphone">iphone</option>
					<option value="mac">mac</option>
				</select>
				<form onSubmit={(e) => this.saveHandler(e)}>
					<div className="row">
						<label htmlFor="title">title</label>
						<input onChange={(e) => this.changeTitle(e)} value={this.state.page.title} type="text"
						       id="title"/>
					</div>
					<div className="row">
						<label htmlFor="content">Content</label>
						<textarea onChange={(e) => this.changeContent(e)} id="content" value={this.state.page.content}/>
					</div>
					<button className="save">SAVE</button>
				</form>
			</Fragment>
		);
		return this.state.loading ? <Spinner/> : info;
	}
}

export default Admin;