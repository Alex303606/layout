import axios from 'axios'

const instance = axios.create({
	baseURL: 'https://layout-a4216.firebaseio.com/'
});

export default instance;