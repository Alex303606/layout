import React, {Component} from 'react';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Content from "./containers/Content/Content";
import Admin from "./containers/Admin/Admin";

class App extends Component {
	render() {
		return (
			<Layout>
				<Switch>
					<Route exact path="/admin" component={Admin}/>
					<Route exact path="/" component={Content}/>
					<Route exact path="/:id" component={Content}/>
				</Switch>
			</Layout>
		);
	}
}

export default App;
