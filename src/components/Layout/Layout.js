import React,{Fragment} from 'react'
import Toolbar from "../Navigations/Toolbar/Toolbar";
import './Layout.css'
const Layout = props => (
	<Fragment>
		<Toolbar/>
		<main className="Layout-Content">
			{props.children}
		</main>
		<footer></footer>
	</Fragment>
);

export default Layout;