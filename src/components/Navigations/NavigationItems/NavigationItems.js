import React from 'react'
import './NavigationItems.css'
import NavigationItem from "./NavigationItem/NavigationItem";
const NavigationItems = () => (
	<ul className="NavigationItems">
		<NavigationItem exact to="/about">About</NavigationItem>
		<NavigationItem exact to="/contacts">Contacts</NavigationItem>
		<NavigationItem exact to="/mac">Mac</NavigationItem>
		<NavigationItem exact to="/iphone">Iphone</NavigationItem>
		<NavigationItem exact to="/ipad">Ipad</NavigationItem>
		<NavigationItem exact to="/admin">Admin</NavigationItem>
	</ul>
);

export default NavigationItems;