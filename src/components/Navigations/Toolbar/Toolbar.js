import React from 'react'
import './Toolbar.css'
import Logo from "../../UI/Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";

const Toolbar = () => (
	<header className="Toolbar">
		<div className="Tollbar-logo">
			<Logo/>
		</div>
		<nav><NavigationItems/></nav>
	</header>
);
export default Toolbar;