import React from 'react';
import logo from '../../../assets/images/logo.png';
import './Logo.css'
const Logo = () => (
	<div className="Logo">
		<img src={logo} alt="MyBurger"/>
	</div>
);

export default Logo;